﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D collider){
		
		EnemyScript enemy = collider.GetComponent<EnemyScript> ();
		if (enemy != null) {
			if (enemy.MoveToPlayer ())
				enemy.transform.parent = transform.parent;
		}
	}
}
