﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

	public int point = 1;
	public bool advancedMovement = false;

	// Use this for initialization
	bool startMove = false;
	WeaponScript weapon;

	void Start () {
		weapon = GetComponentInChildren<WeaponScript> ();
	}

	void OnBecameInvisible() {
		Destroy (gameObject);
	}
		
	// Update is called once per frame
	void Update () {
		if (weapon != null){
			if(Random.Range (0f, 1f) <= 0.01)
				weapon.Attack (true);
		}
		if (startMove && advancedMovement) {
			AdvancedMove ();
		}
	}
	void AdvancedMove () {
		Vector2 oldDirection, newDirection;
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		MoveScript move = GetComponent<MoveScript> ();
		if (player != null && move != null) {
			oldDirection = move.direction;
			newDirection = player.transform.position - transform.position;

			newDirection *= 0.002f;
			newDirection += oldDirection;
			newDirection.Normalize ();
			transform.Rotate (0, 0, angle (oldDirection, newDirection));
			move.direction = newDirection;
		}
	}
	public bool MoveToPlayer () {
		Vector2 direction;
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		MoveScript move = GetComponent<MoveScript> ();
		if (player != null && move != null) {
			startMove = true;
			direction = player.transform.position - transform.position;
			direction.Normalize ();

			transform.Rotate (0, 0, angle (Vector2.down, direction));

			move.direction = direction;
			return true;
		} else {
			return false;
		}
	}
	float angle (Vector2 oldDirection, Vector2 newDirection) {
		float res = 0;

		res = Vector2.Angle (oldDirection, newDirection);
		if (newDirection.x < 0)
			res = -res;
		return res;
	}
}
