﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScript1 : MonoBehaviour {

	public Transform EnemySquadTransform;
	public int level = 1;
	public float timeShowState = 2f;
	// Use this for initialization

	Text score;
	Text state;
	GameObject player;
	bool gameover;
	bool levelComplite;
	private Transform enemySquat;
	void Start () {
		
		GameObject parent = GameObject.Find ("2-Foreground");
		enemySquat =  Instantiate (EnemySquadTransform, new Vector3 (0, 7, 0), Quaternion.identity, parent.transform) as Transform;

		player = GameObject.FindWithTag ("Player");
		state = GameObject.Find ("State").GetComponent<Text> ();
		score = GameObject.Find ("Score").GetComponent<Text> ();
		gameover = false;
		levelComplite = false;
		score.text = "Score : 0";
		state.text = "Level " + level;
	}
	
	// Update is called once per frame
	void Update () {
		ShowState ();
		showScore ();
		if (player == null && !gameover) {
			state.text = "Game Over!";
			timeShowState = 3f;
			gameover = true;
		} else if (enemySquat.childCount == 0 && !levelComplite) {
			state.text = "Level Complite!";
			timeShowState = 3f;
			levelComplite = true;
		}
	}

	void ShowState () {
		if (timeShowState <= 0) {
			if (gameover) {
				SceneManager.LoadScene (0);
			} else if (levelComplite) {
				//загружаем новых игроков
				Destroy(enemySquat.gameObject);
				GameObject parent = GameObject.Find ("2-Foreground");
				enemySquat = Instantiate (EnemySquadTransform, new Vector3 (0, 7, 0) + parent.transform.position, Quaternion.identity, parent.transform) as Transform;
				level++;
				levelComplite = false;
				state.text = "Level " + level;
				timeShowState = 2f;
			}
			state.gameObject.SetActive (false);
		} else {
			timeShowState -= Time.deltaTime;
			state.gameObject.SetActive (true);
		}
	}
	void showScore () {
		score.text = "Score : " + PointsScript.Instance.point;
	}
}
