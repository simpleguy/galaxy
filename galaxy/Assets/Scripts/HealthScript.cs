﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {

	public ParticleSystem explosion;

	public int health = 1;
	public bool isEnemy = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D (Collider2D collider)
	{
		ShotScript shot = collider.gameObject.GetComponent<ShotScript> ();
		if (shot != null) {
			if(shot.isEnemyShot != isEnemy){
				Damage (shot.damage);
			}
		}
	}
	void OnCollisionEnter2D (Collision2D collision){
		HealthScript enemy = collision.gameObject.GetComponent<HealthScript> ();
		if (enemy != null && enemy.isEnemy != isEnemy) {
			Death(); //перманентная смерть
		}
	}
	void Damage (int damage)
	{
		health -= damage;
		if (health <= 0) {
			Death();
		}
	}

	void Death(){
		if (isEnemy) {
			EnemyScript enemy = transform.GetComponentInChildren<EnemyScript> ();
			if(enemy != null)
				PointsScript.Instance.point += enemy.point;
		}
		ParticleSystem part = Instantiate(explosion, transform.position, transform.rotation);
		if (part != null) {
			part.Play ();
			Destroy (part.gameObject, 1);
		}
		Destroy (gameObject);

	}
}
