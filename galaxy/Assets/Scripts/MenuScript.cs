﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System;

public class MenuScript : MonoBehaviour {

	GameObject inputPanel;
	GameObject[] panels;
	string path;

	SaveList saveList; //удалить

	int newScore;
	// Use this for initialization

	void Start () {
		
		saveList = new SaveList (); //список сохранений
		path = Path.Combine (Application.dataPath, "Save.json"); //путь сохранения
		inputPanel = GameObject.Find ("InputPanel");
		inputPanel.SetActive (false);

		if (File.Exists (path)) {
			saveList = SaveList.Load(path);
		} else {
			saveList.Generate();
		}

		if (PointsScript.Instance != null) {
			if (PointsScript.Instance.point > saveList.GetScore (9)) {
				inputPanel.gameObject.SetActive (true);
				Text score =  inputPanel.transform.FindChild ("Score").GetComponent<Text> ();
				newScore = PointsScript.Instance.point; 
				score.text = newScore.ToString();
			}
			Destroy (PointsScript.Instance.gameObject); //он нам больше не нужен
		}

		panels = GameObject.FindGameObjectsWithTag ("ScorePanel"); 

		//сортировка понелей по номеру
		Array.Sort<GameObject> (panels, delegate ( GameObject panel1, GameObject panel2){
			string val1 = panel1.transform.GetChild(0).GetComponent<Text>().text;
			string val2 = panel2.transform.GetChild(0).GetComponent<Text>().text;
			int numb1, numb2;
			int.TryParse(val1, out numb1);
			int.TryParse(val2, out numb2);
			return numb1.CompareTo(numb2);
		});

	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < panels.Length; i++) {
			panels [i].transform.GetChild (1).GetComponent<Text> ().text = saveList.GetName (i);
			panels [i].transform.GetChild (2).GetComponent<Text> ().text = saveList.GetScore (i).ToString();
		}
	}

	public void StartGame()
	{
		SceneManager.LoadScene (1);
		saveList.Save(path);
	}

	public void ExitGame() {
		Application.Quit ();
		saveList.Save(path);
	}

	public void EnterName() {
		Text inputName = GameObject.Find ("InputName").GetComponent<Text>();
		saveList.SetBack(inputName.text, newScore);
		inputPanel.SetActive (false);
	}
}



[Serializable]
public class SaveList {
	public List<Save> saveList;

	public SaveList () {
		saveList = new List<Save>();
	}

	public string GetName(int index) {
		string res = "";
		res = saveList [index].Name;
		return res;
	}
	public int GetScore(int index) {
		int res = 0;
		res = saveList [index].Score;
		return res;
	}

	public void SetBack(string name, int score) {
		saveList.Add (new Save {Name = name, Score = score });
		Sort ();
		saveList.RemoveAt (saveList.Count - 1);
	}

	public void Sort () {
		saveList.Sort ( delegate (Save sv1, Save sv2) {
			return sv2.Score.CompareTo(sv1.Score);
		});
	}
	public void Save (string pathFile) {
		File.WriteAllText (pathFile, JsonUtility.ToJson (this));
	}
	public static SaveList Load (string pathFile) {
		return JsonUtility.FromJson<SaveList> (File.ReadAllText(pathFile));
	}
	public void Generate () {
		saveList.Add (new Save {Name = "Great Nerd", Score = 10001023});
		saveList.Add (new Save {Name = "AAA", Score = 1023});
		saveList.Add (new Save {Name = "simple guy", Score = 526});
		saveList.Add (new Save {Name = "Keanu Reeves", Score = 68});
		saveList.Add (new Save {Name = "Harrison Ford", Score = 55});
		saveList.Add (new Save {Name = "Johnny Depp", Score = 26});
		saveList.Add (new Save {Name = "Robert Downey", Score = 25});
		saveList.Add (new Save {Name = "Hugh Jackman", Score = 85});
		saveList.Add (new Save {Name = "Keira Knightley", Score = 35});
		saveList.Add (new Save {Name = "Natalie Portman", Score = 35});
		Sort ();
	}
}

[Serializable]
public class Save {
	public string Name;
	public int Score;
}