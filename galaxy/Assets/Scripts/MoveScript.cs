﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour {

	public float speed = 10;
	public Vector2 direction = new Vector2(0, 0);
	private Vector2 movement;
	private Rigidbody2D thisObject;
	// Use this for initialization
	void Start () {
		thisObject = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		movement = new Vector2 (speed * direction.x, speed * direction.y);
	}
	void FixedUpdate() {
		thisObject.velocity = movement;
	}
}
