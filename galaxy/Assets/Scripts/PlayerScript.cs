﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	Rigidbody2D player;
	WeaponScript weapon;

	public float speed = 10;

	private Vector2 movement;
	private Vector2 tmpMousPos;
	// Use this for initialization
	void Start () {
		player = GetComponent<Rigidbody2D> ();
		weapon = GetComponentInChildren<WeaponScript> ();
		tmpMousPos = Input.mousePosition;
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector2 mous = Input.mousePosition;
		movement = new Vector2 ((mous.x - tmpMousPos.x) * speed, 0);
		tmpMousPos = mous;
		if (Input.GetMouseButtonDown (0)) {
			if (weapon != null) {
				weapon.Attack (false);
			}
		}

		var dist = (transform.position - Camera.main.transform.position).z;
		var leftBorder = Camera.main.ViewportToWorldPoint (new Vector3(0, 0, dist)).x;
		var rightBorder = Camera.main.ViewportToWorldPoint (new Vector3(1, 0, dist)).x;
		transform.position = new Vector3 (
			Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
			transform.position.y,
			transform.position.z
		);
	}
	void FixedUpdate()
	{
		player.velocity = movement;
	}
}
