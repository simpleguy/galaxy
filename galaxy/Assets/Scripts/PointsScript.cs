﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsScript : MonoBehaviour {

	public static PointsScript Instance;
	public int point = 0;
	// Use this for initialization
	void Awake () {
		if (Instance != null) {
			Debug.LogError ("несколько экземляров");
		}
		Instance = this;
	}

	void Start () {
		DontDestroyOnLoad (this);
	}

	// Update is called once per frame
	void Update () {
		
	}

}
