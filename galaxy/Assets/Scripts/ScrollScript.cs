﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollScript : MonoBehaviour {
	public Vector2 direction = new Vector2(0, 0);
	public float speed = 0;
	public bool isLincedCamera = false;
	public bool isLooping = false;

	Transform background0;
	Transform background1;
	// Use this for initialization
	void Start () {
		if (isLooping) {
			background0 = transform.GetChild (0);
			background1 = transform.GetChild (1);
		}
	}
	// Update is called once per frame
	void Update () {
		Vector3 movement = new Vector3 (direction.x * speed, direction.y * speed, 0);

		movement *= Time.deltaTime;
		transform.Translate (movement);

		if (isLincedCamera) {
			Camera.main.transform.Translate (movement);
		}

		if (isLooping) {
			if (Camera.main.transform.position.y - background0.position.y > 30)
				background0.Translate (0, 60, 0);
			
			if (Camera.main.transform.position.y - background1.position.y > 30)
				background1.Translate (0, 60, 0);
		}
	}
}
