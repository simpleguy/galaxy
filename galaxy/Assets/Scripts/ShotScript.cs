﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotScript : MonoBehaviour {

	public int damage = 1;
	public bool isEnemyShot = false;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 5);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnBecameInvisible() {
		Destroy (gameObject);
	}

	void OnTriggerEnter2D (Collider2D collider){
		HealthScript hp = collider.GetComponent<HealthScript> ();
		if (hp != null && hp.isEnemy != isEnemyShot) {
			Destroy (gameObject);
		}
	}
}
