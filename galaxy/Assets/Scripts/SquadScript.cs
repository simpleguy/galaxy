﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadScript : MonoBehaviour {
	
	// Use this for initialization
	float timeoutMoveDown;
	float timeoutMoveSide;
	Vector2 movement;

	void Awake () {
		timeoutMoveDown = 6f;
		movement = new Vector2 (0.05F, 0);
	}
	void Start () {
		
	}


	// Update is called once per frame
	void Update () {
		MoveSideways ();

		if (timeoutMoveDown <= 0) {
			transform.Translate (new Vector3 (0, -0.5f));
			timeoutMoveDown = 3f;
		} else {
			timeoutMoveDown -= Time.deltaTime;
		}

	}

	void MoveSideways () {
		if (CanMoveSideways ()) {
			transform.Translate (movement);
		} else {
			ReverseMove ();
		}
	}
	
	bool CanMoveSideways() {
		float margen = 5f;
		if (movement.x > 0 && transform.position.x >= margen) {
			return false;
		} else if (movement.x < 0 && transform.position.x <= -margen) {
			return false;
		} else {
			return true;
		}
	}
	void ReverseMove () {
		movement.x = -movement.x;
	}
}
