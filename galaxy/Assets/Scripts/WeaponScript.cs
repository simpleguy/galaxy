﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour {

	public Transform shot;

	GameObject parent;
	public float shotRate = 0.25f;
	private float shotCooldown;
	// Use this for initialization
	void Start () {
		parent = GameObject.Find ("2-Foreground");
		shotCooldown = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (shotCooldown > 0) {
			shotCooldown -= Time.deltaTime;
		}
	}

	public void Attack(bool isEnemy) {
		if (CanAttack ()) {
			shotCooldown = shotRate;
			Instantiate (shot, transform.position, Quaternion.identity, parent.transform);
		}	
	}
	bool CanAttack() {
		return shotCooldown <= 0f;
	}
}
